package com.flight.flight.application.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class AirportDTO {
    Long id;
    String name;
    String shortName;
    String city;
    String country;
}
