package com.flight.flight.application.service;

import com.flight.flight.application.exception.EmailAlreadyExistsException;
import com.flight.flight.application.exception.NotFoundException;
import com.flight.flight.application.dto.TokenDTO;
import com.flight.flight.domain.entity.token.Token;
import com.flight.flight.domain.entity.token.TokenType;
import com.flight.flight.domain.entity.user.Role;
import com.flight.flight.domain.entity.user.User;
import com.flight.flight.domain.repository.TokenRepository;
import com.flight.flight.domain.service.AuthService;
import com.flight.flight.presentation.config.JwtService;
import com.flight.flight.presentation.presenter.auth.SignInRequest;
import com.flight.flight.domain.repository.UserRepository;
import com.flight.flight.presentation.presenter.auth.SignUpRequest;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final TokenRepository tokenRepository;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;

    @SneakyThrows
    @Override
    public TokenDTO signIn(SignInRequest signInRequest) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        signInRequest.getEmail(),
                        signInRequest.getPassword()
                )
        );
        var user = userRepository.findByEmail(signInRequest.getEmail()).orElseThrow(NotFoundException::new);
        var accessToken = jwtService.generateToken(user);
        saveUserToken(user, accessToken);

        return TokenDTO.builder().accessToken(accessToken).build();
    }

    @SneakyThrows
    @Override
    public TokenDTO signUp(SignUpRequest signUpRequest) {
        var user = User.builder()
                .name(signUpRequest.getName())
                .surname(signUpRequest.getSurname())
                .email(signUpRequest.getEmail())
                .password(passwordEncoder.encode(signUpRequest.getPassword()))
                .role(Role.USER)
                .build();
        if (userRepository.findByEmail(user.getEmail()).isPresent())
            throw new EmailAlreadyExistsException();
        var savedUser = userRepository.save(user);
        var accessToken = jwtService.generateToken(savedUser);

        saveUserToken(savedUser, accessToken);
        return TokenDTO.builder().accessToken(accessToken).build();
    }

    private void saveUserToken(User user, String token) {
        var tokenEntity = Token
                .builder()
                .user(user)
                .token(token)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false).build();
        tokenRepository.save(tokenEntity);
    }
}
