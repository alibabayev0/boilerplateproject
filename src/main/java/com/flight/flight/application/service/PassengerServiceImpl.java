package com.flight.flight.application.service;

import com.flight.flight.application.dto.PassengerDTO;
import com.flight.flight.domain.entity.PassengerEntity;
import com.flight.flight.domain.repository.PassengerRepository;
import com.flight.flight.domain.service.PassengerService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PassengerServiceImpl implements PassengerService {
    @Autowired
    private PassengerRepository passengerRepository;


    @SneakyThrows
    public Optional<PassengerDTO> findById(Long id) {

        return passengerRepository.findById(id)
                .map(passenger -> PassengerDTO.builder()
                        .id(passenger.getId())
                        .name(passenger.getName())
                        .surname(passenger.getSurname())
                        .email(passenger.getEmail())
                        .country(passenger.getCountry())
                        .passportNumber(passenger.getPassportNumber())
                        .build());
    }

    @SneakyThrows
    public List<PassengerDTO> findByName(String name) {
        List<PassengerEntity> passenger = passengerRepository.findByName(name);

        return passenger.stream()
                .map(PassengerEntity -> PassengerDTO.builder()
                        .id(PassengerEntity.getId())
                        .name(PassengerEntity.getName())
                        .surname(PassengerEntity.getSurname())
                        .email(PassengerEntity.getEmail())
                        .country(PassengerEntity.getCountry())
                        .passportNumber(PassengerEntity.getPassportNumber())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PassengerDTO> findBySurname(String surname) {
        List<PassengerEntity> passenger = passengerRepository.findByName(surname);

        return passenger.stream()
                .map(PassengerEntity -> PassengerDTO.builder()
                        .id(PassengerEntity.getId())
                        .name(PassengerEntity.getName())
                        .surname(PassengerEntity.getSurname())
                        .email(PassengerEntity.getEmail())
                        .country(PassengerEntity.getCountry())
                        .passportNumber(PassengerEntity.getPassportNumber())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PassengerDTO> findByEmail(String email) {
        List<PassengerEntity> passenger = passengerRepository.findByName(email);

        return passenger.stream()
                .map(PassengerEntity -> PassengerDTO.builder()
                        .id(PassengerEntity.getId())
                        .name(PassengerEntity.getName())
                        .surname(PassengerEntity.getSurname())
                        .email(PassengerEntity.getEmail())
                        .country(PassengerEntity.getCountry())
                        .passportNumber(PassengerEntity.getPassportNumber())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PassengerDTO> findByCountry(String country) {
        List<PassengerEntity> passenger = passengerRepository.findByName(country);

        return passenger.stream()
                .map(PassengerEntity -> PassengerDTO.builder()
                        .id(PassengerEntity.getId())
                        .name(PassengerEntity.getName())
                        .surname(PassengerEntity.getSurname())
                        .email(PassengerEntity.getEmail())
                        .country(PassengerEntity.getCountry())
                        .passportNumber(PassengerEntity.getPassportNumber())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PassengerDTO> findByPassportNumber(String passportNumber) {
        List<PassengerEntity> passenger = passengerRepository.findByName(passportNumber);

        return passenger.stream()
                .map(PassengerEntity -> PassengerDTO.builder()
                        .id(PassengerEntity.getId())
                        .name(PassengerEntity.getName())
                        .surname(PassengerEntity.getSurname())
                        .email(PassengerEntity.getEmail())
                        .country(PassengerEntity.getCountry())
                        .passportNumber(PassengerEntity.getPassportNumber())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PassengerDTO> findAll() {
        return passengerRepository.findAll().stream()
                .map(PassengerEntity -> PassengerDTO.builder()
                        .id(PassengerEntity.getId())
                        .name(PassengerEntity.getName())
                        .surname(PassengerEntity.getSurname())
                        .email(PassengerEntity.getEmail())
                        .country(PassengerEntity.getCountry())
                        .passportNumber(PassengerEntity.getPassportNumber())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public PassengerDTO save(PassengerDTO passengerDTO) {
        PassengerEntity passenger = PassengerEntity.builder()
                .name(passengerDTO.getName())
                .surname(passengerDTO.getSurname())
                .email(passengerDTO.getEmail())
                .country(passengerDTO.getCountry())
                .passportNumber(passengerDTO.getPassportNumber())
                .build();

        passenger = passengerRepository.save(passenger);

        return PassengerDTO.builder()
                .id(passenger.getId())
                .name(passenger.getName())
                .surname(passenger.getSurname())
                .email(passenger.getEmail())
                .country(passenger.getCountry())
                .passportNumber(passenger.getPassportNumber())
                .build();
    }

    public PassengerDTO update(Long id, PassengerDTO passengerDTO) throws EntityNotFoundException{
        PassengerEntity passenger = passengerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Passenger is not found" + id));

        passenger.setName(passengerDTO.getName());
        passenger.setSurname(passengerDTO.getSurname());
        passenger.setEmail(passengerDTO.getEmail());
        passenger.setCountry(passengerDTO.getCountry());
        passenger.setPassportNumber(passengerDTO.getPassportNumber());

        PassengerEntity updatedPassenger = passengerRepository.save(passenger);

        return PassengerDTO.builder()
                .id(updatedPassenger.getId())
                .name(updatedPassenger.getName())
                .surname(updatedPassenger.getSurname())
                .email(updatedPassenger.getEmail())
                .country(updatedPassenger.getCountry())
                .passportNumber(updatedPassenger.getPassportNumber())
                .build();

    }

    @SneakyThrows
    public void deleteById(Long id) {
        passengerRepository.deleteById(id);
    }

    @SneakyThrows
    public boolean existsById(Long id) {
        return passengerRepository.existsById(id);
    }
}
