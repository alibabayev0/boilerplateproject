package com.flight.flight.application.service;

import com.flight.flight.application.dto.PlaneDTO;
import com.flight.flight.domain.entity.PlaneEntity;
import com.flight.flight.domain.repository.PlaneRepository;
import com.flight.flight.domain.service.PlaneService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PlaneServiceImpl implements PlaneService {
    @Autowired
    private PlaneRepository planeRepository;


    @SneakyThrows
    public Optional<PlaneDTO> findById(Long id) {

        return planeRepository.findById(id)
                .map(plane -> PlaneDTO.builder()
                        .id(plane.getId())
                        .name(plane.getName())
                        .numberOfSeats(plane.getNumberOfSeats())
                        .nameOfCompany(plane.getNameOfCompany())
                        .build());
    }

    @SneakyThrows
    public List<PlaneDTO> findByName(String name) {
        List<PlaneEntity> plane = planeRepository.findByName(name);

        return plane.stream()
                .map(PlaneEntity -> PlaneDTO.builder()
                        .id(PlaneEntity.getId())
                        .name(PlaneEntity.getName())
                        .numberOfSeats(PlaneEntity.getNumberOfSeats())
                        .nameOfCompany(PlaneEntity.getNameOfCompany())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PlaneDTO> findByNumberOfSeats(Integer numberOfSeats) {
        List<PlaneEntity> plane = planeRepository.findByNumberOfSeats(numberOfSeats);

        return plane.stream()
                .map(PlaneEntity -> PlaneDTO.builder()
                        .id(PlaneEntity.getId())
                        .name(PlaneEntity.getName())
                        .numberOfSeats(PlaneEntity.getNumberOfSeats())
                        .nameOfCompany(PlaneEntity.getNameOfCompany())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PlaneDTO> findByNameOfCompany(String nameOfCompany) {
        List<PlaneEntity> plane = planeRepository.findByNameOfCompany(nameOfCompany);

        return plane.stream()
                .map(PlaneEntity -> PlaneDTO.builder()
                        .id(PlaneEntity.getId())
                        .name(PlaneEntity.getName())
                        .numberOfSeats(PlaneEntity.getNumberOfSeats())
                        .nameOfCompany(PlaneEntity.getNameOfCompany())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PlaneDTO> findAll() {
        return planeRepository.findAll().stream()
                .map(PlaneEntity -> PlaneDTO.builder()
                        .id(PlaneEntity.getId())
                        .name(PlaneEntity.getName())
                        .numberOfSeats(PlaneEntity.getNumberOfSeats())
                        .nameOfCompany(PlaneEntity.getNameOfCompany())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public PlaneDTO save(PlaneDTO planeDTO) {
        PlaneEntity plane = PlaneEntity.builder()
                .name(planeDTO.getName())
                .numberOfSeats(planeDTO.getNumberOfSeats())
                .nameOfCompany(planeDTO.getNameOfCompany())
                .build();

        plane = planeRepository.save(plane);

        return PlaneDTO.builder()
                .id(plane.getId())
                .name(plane.getName())
                .numberOfSeats(plane.getNumberOfSeats())
                .nameOfCompany(plane.getNameOfCompany())
                .build();
    }

    public PlaneDTO update(Long id, PlaneDTO planeDTO) throws EntityNotFoundException{
        PlaneEntity plane = planeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Plane is not found" + id));

        plane.setName(planeDTO.getName());
        plane.setNumberOfSeats(planeDTO.getNumberOfSeats());
        plane.setNameOfCompany(planeDTO.getNameOfCompany());

        PlaneEntity updatedPlane = planeRepository.save(plane);

        return PlaneDTO.builder()
                .id(updatedPlane.getId())
                .name(updatedPlane.getName())
                .numberOfSeats(updatedPlane.getNumberOfSeats())
                .nameOfCompany(updatedPlane.getNameOfCompany())
                .build();

    }

    @SneakyThrows
    public void deleteById(Long id) {
        planeRepository.deleteById(id);
    }

    @SneakyThrows
    public boolean existsById(Long id) {
        return planeRepository.existsById(id);
    }
}
