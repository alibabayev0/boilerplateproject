package com.flight.flight.application.service;

import com.flight.flight.application.dto.TicketDTO;
import com.flight.flight.domain.entity.*;
import com.flight.flight.domain.repository.TicketRepository;
import com.flight.flight.domain.service.TicketService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {
    @Autowired
    private TicketRepository ticketRepository;


    @SneakyThrows
    public Optional<TicketDTO> findById(Long id) {

        return ticketRepository.findById(id)
                .map(ticket -> TicketDTO.builder()
                        .id(ticket.getId())
                        .flight(ticket.getFlight())
                        .passenger(ticket.getPassenger())
                        .transaction(ticket.getTransaction())
                        .build());
    }

    @SneakyThrows
    public List<TicketDTO> findByFlightId(FlightEntity flightId) {
        List<TicketEntity> ticket = ticketRepository.findByFlightId(flightId);

        return ticket.stream()
                .map(TicketEntity -> TicketDTO.builder()
                        .id(TicketEntity.getId())
                        .flight(TicketEntity.getFlight())
                        .passenger(TicketEntity.getPassenger())
                        .transaction(TicketEntity.getTransaction())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TicketDTO> findByPassengerId(PassengerEntity passengerId) {
        List<TicketEntity> ticket = ticketRepository.findByPassengerId(passengerId);

        return ticket.stream()
                .map(TicketEntity -> TicketDTO.builder()
                        .id(TicketEntity.getId())
                        .flight(TicketEntity.getFlight())
                        .passenger(TicketEntity.getPassenger())
                        .transaction(TicketEntity.getTransaction())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TicketDTO> findByTransactionId(TransactionEntity transactionId) {
        List<TicketEntity> ticket = ticketRepository.findByTransactionId(transactionId);

        return ticket.stream()
                .map(TicketEntity -> TicketDTO.builder()
                        .id(TicketEntity.getId())
                        .flight(TicketEntity.getFlight())
                        .passenger(TicketEntity.getPassenger())
                        .transaction(TicketEntity.getTransaction())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TicketDTO> findAll() {
        return ticketRepository.findAll().stream()
                .map(TicketEntity -> TicketDTO.builder()
                        .id(TicketEntity.getId())
                        .flight(TicketEntity.getFlight())
                        .passenger(TicketEntity.getPassenger())
                        .transaction(TicketEntity.getTransaction())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public TicketDTO save(TicketDTO ticketDTO) {
        TicketEntity ticket = TicketEntity.builder()
                .flight(ticketDTO.getFlight())
                .passenger(ticketDTO.getPassenger())
                .transaction(ticketDTO.getTransaction())
                .build();

        ticket = ticketRepository.save(ticket);

        return TicketDTO.builder()
                .id(ticket.getId())
                .flight(ticket.getFlight())
                .passenger(ticket.getPassenger())
                .transaction(ticket.getTransaction())
                .build();
    }

    public TicketDTO update(Long id, TicketDTO ticketDTO) throws EntityNotFoundException{
        TicketEntity ticket = ticketRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Ticket is not found" + id));

        ticket.setFlight(ticketDTO.getFlight());
        ticket.setPassenger(ticketDTO.getPassenger());
        ticket.setTransaction(ticketDTO.getTransaction());

        TicketEntity updatedTicket = ticketRepository.save(ticket);

        return TicketDTO.builder()
                .id(updatedTicket.getId())
                .flight(updatedTicket.getFlight())
                .passenger(updatedTicket.getPassenger())
                .transaction(updatedTicket.getTransaction())
                .build();

    }

    @SneakyThrows
    public void deleteById(Long id) {
        ticketRepository.deleteById(id);
    }

    @SneakyThrows
    public boolean existsById(Long id) {
        return ticketRepository.existsById(id);
    }
}
