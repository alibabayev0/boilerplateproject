package com.flight.flight.application.service;

import com.flight.flight.application.dto.TransactionDTO;
import com.flight.flight.domain.entity.Status.Status;
import com.flight.flight.domain.entity.TransactionEntity;
import com.flight.flight.domain.repository.TransactionRepository;
import com.flight.flight.domain.service.TransactionService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;


    @SneakyThrows
    public Optional<TransactionDTO> findById(Long id) {

        return transactionRepository.findById(id)
                .map(transaction -> TransactionDTO.builder()
                        .id(transaction.getId())
                        .rrn(transaction.getRrn())
                        .invoiceCode(transaction.getInvoiceCode())
                        .price(transaction.getPrice())
                        .paymentDate(transaction.getPaymentDate())
                        .status(transaction.getStatus())
                        .createdAt(transaction.getCreatedAt())
                        .updatedAt(transaction.getUpdatedAt())
                        .flightId(transaction.getFlightId())
                        .passengerId(transaction.getPassengerId())
                        .build());
    }

    @SneakyThrows
    public List<TransactionDTO> findByRrn(String rrn) {
        List<TransactionEntity> transaction = transactionRepository.findByRrn(rrn);

        return transaction.stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TransactionDTO> findByInvoiceCode(String invoiceCode) {
        List<TransactionEntity> transaction = transactionRepository.findByInvoiceCode(invoiceCode);

        return transaction.stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TransactionDTO> findByPrice(Double price) {
        List<TransactionEntity> transaction = transactionRepository.findByPrice(price);

        return transaction.stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TransactionDTO> findByPaymentDate(Date paymentDate) {
        List<TransactionEntity> transaction = transactionRepository.findByPaymentDate(paymentDate);

        return transaction.stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TransactionDTO> findByStatus(Status status) {
        List<TransactionEntity> transaction = transactionRepository.findByStatus(status);

        return transaction.stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TransactionDTO> findByCreatedAt(Date createdAt) {
        List<TransactionEntity> transaction = transactionRepository.findByCreatedAt(createdAt);

        return transaction.stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TransactionDTO> findByUpdatedAt(Date updatedAt) {
        List<TransactionEntity> transaction = transactionRepository.findByUpdatedAt(updatedAt);

        return transaction.stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TransactionDTO> findByFlightId(Integer flightId) {
        List<TransactionEntity> transaction = transactionRepository.findByFlightId(flightId);

        return transaction.stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TransactionDTO> findByPassengerId(Integer passengerId) {
        List<TransactionEntity> transaction = transactionRepository.findByPassengerId(passengerId);

        return transaction.stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<TransactionDTO> findAll() {
        return transactionRepository.findAll().stream()
                .map(TransactionEntity -> TransactionDTO.builder()
                        .id(TransactionEntity.getId())
                        .rrn(TransactionEntity.getRrn())
                        .invoiceCode(TransactionEntity.getInvoiceCode())
                        .price(TransactionEntity.getPrice())
                        .paymentDate(TransactionEntity.getPaymentDate())
                        .status(TransactionEntity.getStatus())
                        .createdAt(TransactionEntity.getCreatedAt())
                        .updatedAt(TransactionEntity.getUpdatedAt())
                        .flightId(TransactionEntity.getFlightId())
                        .passengerId(TransactionEntity.getPassengerId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public TransactionDTO save(TransactionDTO transactionDTO) {
        TransactionEntity transaction = TransactionEntity.builder()
                .rrn(transactionDTO.getRrn())
                .invoiceCode(transactionDTO.getInvoiceCode())
                .price(transactionDTO.getPrice())
                .paymentDate(transactionDTO.getPaymentDate())
                .status(transactionDTO.getStatus())
                .createdAt(transactionDTO.getCreatedAt())
                .updatedAt(transactionDTO.getUpdatedAt())
                .flightId(transactionDTO.getFlightId())
                .passengerId(transactionDTO.getPassengerId())
                .build();

        transaction = transactionRepository.save(transaction);

        return TransactionDTO.builder()
                .id(transaction.getId())
                .rrn(transaction.getRrn())
                .invoiceCode(transaction.getInvoiceCode())
                .price(transaction.getPrice())
                .paymentDate(transaction.getPaymentDate())
                .status(transaction.getStatus())
                .createdAt(transaction.getCreatedAt())
                .updatedAt(transaction.getUpdatedAt())
                .flightId(transaction.getFlightId())
                .passengerId(transaction.getPassengerId())
                .build();
    }

    public TransactionDTO update(Long id, TransactionDTO transactionDTO) throws EntityNotFoundException{
        TransactionEntity transaction = transactionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Transaction is not found" + id));

        transaction.setRrn(transactionDTO.getRrn());
        transaction.setInvoiceCode(transactionDTO.getInvoiceCode());
        transaction.setPrice(transactionDTO.getPrice());
        transaction.setPaymentDate(transactionDTO.getPaymentDate());
        transaction.setStatus(transactionDTO.getStatus());
        transaction.setCreatedAt(transactionDTO.getCreatedAt());
        transaction.setUpdatedAt(transactionDTO.getUpdatedAt());
        transaction.setFlightId(transactionDTO.getFlightId());
        transaction.setPassengerId(transactionDTO.getPassengerId());

        TransactionEntity updatedTransaction = transactionRepository.save(transaction);

        return TransactionDTO.builder()
                .id(updatedTransaction.getId())
                .rrn(updatedTransaction.getRrn())
                .invoiceCode(updatedTransaction.getInvoiceCode())
                .price(updatedTransaction.getPrice())
                .paymentDate(updatedTransaction.getPaymentDate())
                .status(updatedTransaction.getStatus())
                .createdAt(updatedTransaction.getCreatedAt())
                .updatedAt(updatedTransaction.getUpdatedAt())
                .flightId(updatedTransaction.getFlightId())
                .passengerId(updatedTransaction.getPassengerId())
                .build();

    }

    @SneakyThrows
    public void deleteById(Long id) {
        transactionRepository.deleteById(id);
    }

    @SneakyThrows
    public boolean existsById(Long id) {
        return transactionRepository.existsById(id);
    }
}
