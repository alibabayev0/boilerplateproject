package com.flight.flight.domain.entity.CabinClass;

public enum CabinClassType {
    ECONOMY,
    BUSINESS,
    FIRST_CLASS
}
