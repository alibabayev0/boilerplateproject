package com.flight.flight.domain.entity;

import com.flight.flight.domain.entity.CabinClass.CabinClassType;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "Prices")
public class PriceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    CabinClassType cabinClass;

    @Column
    Double minMultiplier;

    @Column
    Double maxMultiplier;

}
