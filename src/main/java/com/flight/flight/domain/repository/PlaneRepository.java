package com.flight.flight.domain.repository;

import com.flight.flight.domain.entity.PassengerEntity;
import com.flight.flight.domain.entity.PlaneEntity;
import java.util.List;
import java.util.Optional;

public interface PlaneRepository {
    Optional<PlaneEntity> findById(Long id);

    List<PlaneEntity> findByName(String name);

    List<PlaneEntity> findByNumberOfSeats( Integer numberOfSeats);

    List<PlaneEntity> findByNameOfCompany( String nameOfCompany);

    PlaneEntity save(PlaneEntity plane);

    List<PlaneEntity> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
