package com.flight.flight.domain.repository;

import com.flight.flight.domain.entity.CabinClass.CabinClassType;
import com.flight.flight.domain.entity.PlaneEntity;
import com.flight.flight.domain.entity.PriceEntity;
import java.util.List;
import java.util.Optional;

public interface PriceRepository {
    Optional<PriceEntity> findById(Long id);

    List<PriceEntity> findByCabinClass(CabinClassType cabinClass);

    List<PriceEntity> findByMinMultiplier(Double minMultiplier);

    List<PriceEntity> findByMaxMultiplier(Double maxMultiplier);

    PriceEntity save(PriceEntity price);

    List<PriceEntity> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
