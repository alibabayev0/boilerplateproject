package com.flight.flight.domain.repository;

import com.flight.flight.domain.entity.*;
import java.util.List;
import java.util.Optional;

public interface TicketRepository {
    Optional<TicketEntity> findById(Long id);

    List<TicketEntity> findByFlightId(FlightEntity flightId);

    List<TicketEntity> findByPassengerId(PassengerEntity passengerId);

    List<TicketEntity> findByTransactionId(TransactionEntity transactionId);

    TicketEntity save(TicketEntity ticket);

    List<TicketEntity> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
