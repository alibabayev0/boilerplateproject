package com.flight.flight.domain.service;

import com.flight.flight.application.dto.FlightDTO;
import com.flight.flight.domain.entity.AirportEntity;
import com.flight.flight.domain.entity.PlaneEntity;
import com.flight.flight.domain.entity.PriceEntity;
import com.flight.flight.presentation.presenter.flight.CreateFlightRequest;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface FlightService {
    Optional<FlightDTO> findById(Long id);

    List<FlightDTO> findByDepartureAirport(AirportEntity departureAirport);

    List<FlightDTO> findByArrivalAirport(AirportEntity arrivalAirport);

    List<FlightDTO> findByDepartureDate(Date departureDate);

    List<FlightDTO> findByArrivalDate(Date arrivalDate);

    List<FlightDTO> findByDistance(Integer distance);

    List<FlightDTO> findByFlightTime(Integer flightTime);

    List<FlightDTO> findByPlaneId(PlaneEntity planeId);

    List<FlightDTO> findByPriceId(PriceEntity priceId);

    FlightDTO save(CreateFlightRequest flight);

    FlightDTO update(Long id, FlightDTO flight);

    List<FlightDTO> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
