package com.flight.flight.domain.service;

import com.flight.flight.application.dto.PassengerDTO;
import java.util.List;
import java.util.Optional;

public interface PassengerService {
    Optional<PassengerDTO> findById(Long id);
    List<PassengerDTO> findByName(String name);

    List<PassengerDTO> findBySurname( String surname);

    List<PassengerDTO> findByEmail( String email);

    List<PassengerDTO> findByCountry( String country);

    List<PassengerDTO> findByPassportNumber( String passportNumber);

    PassengerDTO save(PassengerDTO passenger);

    PassengerDTO update(Long id, PassengerDTO passenger);

    List<PassengerDTO> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
