package com.flight.flight.domain.service;

import com.flight.flight.application.dto.TicketDTO;
import com.flight.flight.domain.entity.FlightEntity;
import com.flight.flight.domain.entity.PassengerEntity;
import com.flight.flight.domain.entity.TransactionEntity;
import java.util.List;
import java.util.Optional;

public interface TicketService {
    Optional<TicketDTO> findById(Long id);
    List<TicketDTO> findByFlightId(FlightEntity flightId);

    List<TicketDTO> findByPassengerId(PassengerEntity passengerId);

    List<TicketDTO> findByTransactionId(TransactionEntity transactionId);

    TicketDTO save(TicketDTO ticket);

    TicketDTO update(Long id, TicketDTO ticket);

    List<TicketDTO> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
