package com.flight.flight.domain.service;

import com.flight.flight.application.dto.TransactionDTO;
import com.flight.flight.domain.entity.Status.Status;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TransactionService {
    Optional<TransactionDTO> findById(Long id);
    List<TransactionDTO> findByRrn(String rrn);

    List<TransactionDTO> findByInvoiceCode(String invoiceCode);

    List<TransactionDTO> findByPrice(Double price);

    List<TransactionDTO> findByPaymentDate(Date paymentDate);

    List<TransactionDTO> findByStatus(Status status);

    List<TransactionDTO> findByCreatedAt(Date createdAt);

    List<TransactionDTO> findByUpdatedAt(Date updatedAt);

    List<TransactionDTO> findByFlightId(Integer flightId);

    List<TransactionDTO> findByPassengerId(Integer passengerId);

    TransactionDTO save(TransactionDTO transaction);

    TransactionDTO update(Long id, TransactionDTO transaction);

    List<TransactionDTO> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
