package com.flight.flight.infrastructure.mapper;

import com.flight.flight.application.dto.PriceDTO;
import com.flight.flight.domain.entity.PriceEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PriceMapper {
    static PriceDTO mapPriceEntityToPriceDTO(PriceEntity entity) {
        if (entity == null) {
            return null;
        }

        PriceDTO dto = new PriceDTO();
        dto.setId(entity.getId());
        dto.setCabinClass(entity.getCabinClass());
        dto.setMinMultiplier(entity.getMinMultiplier());
        dto.setMaxMultiplier(entity.getMaxMultiplier());

        return dto;
    }

    static PriceEntity mapPriceDTOToPriceEntity(PriceDTO dto) {
        if (dto == null) {
            return null;
        }

        PriceEntity entity = new PriceEntity();

        entity.setId(dto.getId());
        entity.setCabinClass(dto.getCabinClass());
        entity.setMinMultiplier(dto.getMinMultiplier());
        entity.setMaxMultiplier(dto.getMaxMultiplier());

        return entity;
    }
}
