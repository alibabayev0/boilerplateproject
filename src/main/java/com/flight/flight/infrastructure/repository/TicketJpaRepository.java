package com.flight.flight.infrastructure.repository;

import com.flight.flight.domain.entity.FlightEntity;
import com.flight.flight.domain.entity.PassengerEntity;
import com.flight.flight.domain.entity.TicketEntity;
import com.flight.flight.domain.entity.TransactionEntity;
import com.flight.flight.domain.repository.TicketRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketJpaRepository extends JpaRepository<TicketEntity, Long>, TicketRepository {
    List<TicketEntity> findByFlightId(FlightEntity flightId);

    List<TicketEntity> findByPassengerId(PassengerEntity passengerId);

    List<TicketEntity> findByTransactionId(TransactionEntity transactionId);
}
