package com.flight.flight.presentation.controller;

import com.flight.flight.application.dto.AirportDTO;
import com.flight.flight.application.service.AirportServiceImpl;
import com.flight.flight.domain.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/airports")
public class AirportController {

    @Autowired
    private AirportService airportService;

    public AirportController(AirportServiceImpl airportService) {

        this.airportService = airportService;
    }

    //Find All Airports Method
    @GetMapping
    public ResponseEntity<List<AirportDTO>> findAllAirports() {
        List<AirportDTO> airports = airportService.findAll();
        return ResponseEntity.ok(airports);
    }

    //Find Airport By ID Method
    @GetMapping("/{id}")
    public ResponseEntity<AirportDTO> findAirportById(@PathVariable Long id) {
        Optional<AirportDTO> airport = airportService.findById(id);
        if (airport.isPresent()) {
            return ResponseEntity.ok(airport.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    //Create New Airport Method
    @PostMapping
    public ResponseEntity<AirportDTO> createNewAirport(@RequestBody AirportDTO airportDTO) {
        AirportDTO savedAirport = airportService.save(airportDTO);
        return new ResponseEntity<>(savedAirport, HttpStatus.CREATED);
    }

    //Update Airport By ID Method
    @PutMapping("/{id}")
    public ResponseEntity<AirportDTO> updateAirport(@PathVariable Long id, @RequestBody AirportDTO airportDTO) {
        AirportDTO updatedAirport = airportService.update(id, airportDTO);
        return ResponseEntity.ok(updatedAirport);
    }


    //Delete Airport By ID Method
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAirport(@PathVariable Long id) {
        airportService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}


