package com.flight.flight.presentation.controller;

import com.flight.flight.application.service.PriceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/prices")
public class PriceController {
    @Autowired
    private PriceServiceImpl priceService;

    public PriceController(PriceServiceImpl priceService) {

        this.priceService = priceService;
    }

    //Find All Prices Method
//    @GetMapping
//    public ResponseEntity<List<PriceDTO>> findAllPrices() {
//        List<PriceDTO> prices = priceService.findAll();
//        return ResponseEntity.ok(prices);
//    }

    //Find Price By ID Method
//    @GetMapping("/{id}")
//    public ResponseEntity<PriceDTO> findPriceById(@PathVariable Long id) {
//        Optional<PriceDTO> price = priceService.findById(id);
//        if (price.isPresent()) {
//            return ResponseEntity.ok(price.get());
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }

    //Create New Price Method
//    @PostMapping
//    public ResponseEntity<PriceDTO> createNewPrice(@RequestBody PriceDTO priceDTO) {
//        PriceDTO savedPrice = priceService.save(priceDTO);
//        return new ResponseEntity<>(savedPrice, HttpStatus.CREATED);
//    }

    //Update Price By ID Method
//    @PutMapping("/{id}")
//    public ResponseEntity<PriceDTO> updatePrice(@PathVariable Long id, @RequestBody PriceDTO priceDTO) {
//        PriceDTO updatedPrice = priceService.update(id, priceDTO);
//        return ResponseEntity.ok(updatedPrice);
//    }
//
//
//    //Delete Price By ID Method
//    @DeleteMapping("/{id}")
//    public ResponseEntity<Void> deletePrice(@PathVariable Long id) {
//        priceService.deleteById(id);
//        return ResponseEntity.noContent().build();
//    }
}
