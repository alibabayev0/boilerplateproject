package com.flight.flight.presentation.controller;

import com.flight.flight.application.service.TicketServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tickets")
public class TicketCotroller {
    @Autowired
    private TicketServiceImpl ticketService;

    public TicketCotroller(TicketServiceImpl ticketService) {

        this.ticketService = ticketService;
    }

    //Find All Tickets Method
//    @GetMapping
//    public ResponseEntity<List<TicketDTO>> findAllTickets() {
//        List<TicketDTO> tickets = ticketService.findAll();
//        return ResponseEntity.ok(tickets);
//    }
//
//    @GetMapping("/{id}")
//    public ResponseEntity<TicketDTO> findTicketById(@PathVariable Long id) {
//        Optional<TicketDTO> ticket = ticketService.findById(id);
//        if (ticket.isPresent()) {
//            return ResponseEntity.ok(ticket.get());
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }
//
//    @PostMapping
//    public ResponseEntity<TicketDTO> createNewTicket(@RequestBody TicketDTO ticketDTO) {
//        TicketDTO savedTicket = ticketService.save(ticketDTO);
//        return new ResponseEntity<>(savedTicket, HttpStatus.CREATED);
//    }
//
//    @PutMapping("/{id}")
//    public ResponseEntity<TicketDTO> updateTicket(@PathVariable Long id, @RequestBody TicketDTO ticketDTO) {
//        TicketDTO updatedTicket = ticketService.update(id, ticketDTO);
//        return ResponseEntity.ok(updatedTicket);
//    }
//
//
//    //Delete Ticket By ID Method
//    @DeleteMapping("/{id}")
//    public ResponseEntity<Void> deleteTicket(@PathVariable Long id) {
//        ticketService.deleteById(id);
//        return ResponseEntity.noContent().build();
//    }
}
